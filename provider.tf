terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# provier block
provider "aws" {
  region = "ap-southeast-1"
  profile = "default"
}