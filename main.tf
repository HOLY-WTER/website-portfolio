# s3 bucket
resource "aws_s3_bucket" "my_bucket" {
  bucket = var.bucketname

  tags = {
    Name        = "nammon_bucket"
    #Environment = "Dev"
  }
}
#ownership_controls block
resource "aws_s3_bucket_ownership_controls" "example" {
  bucket = aws_s3_bucket.my_bucket.id

  rule {
    object_ownership = "BucketOwnerPreferred" #only owner can config
  }
}

resource "aws_s3_bucket_public_access_block" "example" { 
  bucket = aws_s3_bucket.my_bucket.id

  # unblocck acl public access
  block_public_acls       = false 
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "example" {
  depends_on = [
    aws_s3_bucket_ownership_controls.example,
    aws_s3_bucket_public_access_block.example,
  ]

  bucket = aws_s3_bucket.my_bucket.id
  acl    = "public-read"
}

resource "aws_s3_object" "index" {
  bucket = aws_s3_bucket.my_bucket.id
  key= "index.html"
  source = "first_portfolio/index.html"
  acl = "public-read"
  content_type = "text/html"
}

resource "aws_s3_object" "error" {
  bucket = aws_s3_bucket.my_bucket.id
  key= "error.html"
  source = "first_portfolio/error.html"
  acl = "public-read"
  content_type = "text/html"
}

resource "aws_s3_object" "files" {
  bucket = aws_s3_bucket.my_bucket.id
  key = "file/Poramin Srithong-Devops-CV.pdf"
  source = "first_portfolio/file/Poramin Srithong-Devops-CV.pdf"
  acl = "public-read"
}



resource "aws_s3_bucket_website_configuration" "website" {
  bucket = aws_s3_bucket.my_bucket.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  
  depends_on = [ aws_s3_bucket_acl.example ] #it will created after bucket has been created
}